FROM node:6

MAINTAINER Andrea Frigido <andrea@frisoft.it>

RUN npm install -g elm@0.18.0
